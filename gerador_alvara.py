#!/usr/bin/python

import os
import sys
from datetime import date
from xlrd import open_workbook, cellname
from PIL import Image, ImageDraw, ImageFont


def gerar_alvara(
    numero_alvara,
    razao_social,
    cnpj,
    inscricao_estadual,
    cod_servico,
    sim_n,
    endereco,
    numero,
    bairro,
    atividade,
    responsabilidade,
    responsavel_tecnico,
    observacoes,
):
    nome_arquivo_alvara = "alvara_{}.png".format(razao_social.replace(" ", "_"))
    imagem_alvara = Image.open("{0}{1}alvara.png".format(os.getcwd(), os.sep))
    imagem_alvara_desenho = ImageDraw.Draw(imagem_alvara)
    fonte_alvara = ImageFont.truetype("arial", 100, encoding="unic")
    imagem_alvara_desenho.text(
        (5169, 950), numero_alvara, font=fonte_alvara, fill=(0, 0, 0)
    )
    imagem_alvara_desenho.text((5489, 1150), ano, font=fonte_alvara, fill=(0, 0, 0))
    imagem_alvara_desenho.text(
        (1729, 1445), razao_social, font=fonte_alvara, fill=(0, 0, 0)
    )
    imagem_alvara_desenho.text((757, 1600), cnpj, font=fonte_alvara, fill=(0, 0, 0))
    imagem_alvara_desenho.text(
        (3053, 1600), inscricao_estadual, font=fonte_alvara, fill=(0, 0, 0)
    )
    imagem_alvara_desenho.text(
        (4838, 1600), cod_servico, font=fonte_alvara, fill=(0, 0, 0)
    )
    imagem_alvara_desenho.text((865, 1745), sim_n, font=fonte_alvara, fill=(0, 0, 0))
    imagem_alvara_desenho.text(
        (1332, 1925), endereco, font=fonte_alvara, fill=(0, 0, 0)
    )
    imagem_alvara_desenho.text((3842, 1920), numero, font=fonte_alvara, fill=(0, 0, 0))
    imagem_alvara_desenho.text((4682, 1920), bairro, font=fonte_alvara, fill=(0, 0, 0))
    imagem_alvara_desenho.text(
        (904, 2060), atividade, font=fonte_alvara, fill=(0, 0, 0)
    )
    imagem_alvara_desenho.text(
        (1521, 2185), responsabilidade, font=fonte_alvara, fill=(0, 0, 0)
    )
    imagem_alvara_desenho.text(
        (4610, 2170), responsavel_tecnico, font=fonte_alvara, fill=(0, 0, 0)
    )
    imagem_alvara_desenho.text(
        (1065, 2365), observacoes, font=fonte_alvara, fill=(0, 0, 0)
    )

    # Data
    imagem_alvara_desenho.text(
        (2570, 3400), date.today().strftime("%d"), font=fonte_alvara, fill=(0, 0, 0),
    )
    imagem_alvara_desenho.text((3300, 3400), mes, font=fonte_alvara, fill=(0, 0, 0))
    imagem_alvara_desenho.text((4555, 3400), ano, font=fonte_alvara, fill=(0, 0, 0))

    imagem_alvara.save('{0}{1}alvaras_gerados{1}{2}'.format(os.getcwd(), os.sep, nome_arquivo_alvara), "PNG", resolution=100.0)

    return nome_arquivo_alvara


if date.today().strftime("%m") == "01":
    mes = "Janeiro"
elif date.today().strftime("%m") == "02":
    mes = "Fevereiro"
elif date.today().strftime("%m") == "03":
    mes = "Março"
elif date.today().strftime("%m") == "04":
    mes = "Abril"
elif date.today().strftime("%m") == "05":
    mes = "Maio"
elif date.today().strftime("%m") == "06":
    mes = "Junho"
elif date.today().strftime("%m") == "07":
    mes = "Julho"
elif date.today().strftime("%m") == "08":
    mes = "Agosto"
elif date.today().strftime("%m") == "09":
    mes = "Setembro"
elif date.today().strftime("%m") == "10":
    mes = "Outubro"
elif date.today().strftime("%m") == "11":
    mes = "Novembro"
elif date.today().strftime("%m") == "12":
    mes = "Dezembro"

ano = date.today().strftime("%Y")


if __name__ == "__main__":

    try:
        planilha_alvara = open_workbook(
            "{0}{1}alvaras_a_serem_gerados.xlsx".format(os.getcwd(), os.sep)
        )
        folha_planilha_alvara = planilha_alvara.sheet_by_index(0)
    except:
        print(
            "Planilha com os dados encontra-se aberta, ou não foi encontrada. Verifique e tente novamente."
        )
        sys.exit(-1)

    print("Gerando alvaras, aguarde ...")
    try:

        if os.path.isdir('alvaras_gerados') == False:
            os.mkdir('alvaras_gerados')

        for i in range(1, folha_planilha_alvara.nrows):
            numero_alvara = str(folha_planilha_alvara.row_values(i)[0])
            razao_social = str(folha_planilha_alvara.row_values(i)[1])
            cnpj = str(folha_planilha_alvara.row_values(i)[2])
            inscricao_estadual = str(folha_planilha_alvara.row_values(i)[3])
            cod_servico = str(folha_planilha_alvara.row_values(i)[4])
            sim_n = str(folha_planilha_alvara.row_values(i)[5])
            endereco = str(folha_planilha_alvara.row_values(i)[6])
            numero = str(folha_planilha_alvara.row_values(i)[7])
            bairro = str(folha_planilha_alvara.row_values(i)[8])
            atividade = str(folha_planilha_alvara.row_values(i)[9])
            responsabilidade = str(folha_planilha_alvara.row_values(i)[10])
            responsavel_tecnico = str(folha_planilha_alvara.row_values(i)[11])
            observacoes = str(folha_planilha_alvara.row_values(i)[12])

            nome_do_alvara = gerar_alvara(
                numero_alvara,
                razao_social,
                cnpj,
                inscricao_estadual,
                cod_servico,
                sim_n,
                endereco,
                numero,
                bairro,
                atividade,
                responsabilidade,
                responsavel_tecnico,
                observacoes,
            )

            print(
                "{} gerado com sucesso ...".format(nome_do_alvara.replace(".png", ""))
            )

            print("Gerando PDF ... ")
            png = Image.open('{0}{1}alvaras_gerados{1}{2}'.format(os.getcwd(), os.sep, nome_do_alvara))
            png.load()

            background = Image.new("RGB", png.size, (255, 255, 255))
            background.paste(png, mask=png.split()[3])  # 3 is the alpha channel
            background.save('{0}{1}alvaras_gerados{1}{2}'.format(os.getcwd(), os.sep, nome_do_alvara.replace(".png", ".pdf")), save_all=True)
        input(
            "Todos alvaras foram gerados com sucesso. Tecle ENTER para continuar ... "
        )
    except:
        print(
            "\n\nERRO!"
        )
        print("Não foi possível gerar o alvara {}".format(nome_do_alvara.split(os.sep)[1]))
        print("Caso existam outros na lista, tente remove-lo para dar sequência.")
        sys.exit(-1)

